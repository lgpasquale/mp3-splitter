Split a given mp3 file into multiple tracks using a file containing the tracklist

Also tags output tracks accordingly.

Needs:
- `python-mutagen`
- `ffmpeg`

```
usage: mp3-splitter.py [-h] [-i INPUT] [-o OUTPUT] [-l TRACK_LIST] [-t TEMPLATE] [--skip-intro]

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT, --input INPUT
                        input mp3 file
  -o OUTPUT, --output OUTPUT
                        output dir
  -l TRACK_LIST, --track-list TRACK_LIST
                        track list file
  -t TEMPLATE, --template TEMPLATE
                        template (default: "%timestamp% - %title%")
  --skip-intro
```
