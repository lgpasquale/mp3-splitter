#!/usr/bin/env python3

import argparse
import os
import re
import subprocess
from pathlib import Path

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input", help="input mp3 file")
parser.add_argument("-o", "--output", help="output dir")
parser.add_argument("-l", "--track-list", help="track list file")
parser.add_argument("-t", "--template", help='template (default: "%%timestamp%% - %%title%%")',
    default="%timestamp% - %title%")
parser.add_argument('--skip-intro', default=False, action='store_true')
args = parser.parse_args()

Path(args.output).mkdir(parents=True, exist_ok=True)

regex_template = args.template.replace("%timestamp%", "(?P<timestamp>[0-9:]+)").replace("%title%", "(?P<title>.*)")
print(regex_template)
segment_times = []
segments = []
with open(args.track_list, 'r') as track_list_file:
    for line in track_list_file:
        results = re.search(regex_template, line)
        timestamp = results.group("timestamp")
        title = results.group("title")
        print("{} - {}".format(timestamp, title))
        segments.append({"timestamp": timestamp, "title": title})

timestamps_string = ",".join([s["timestamp"] for s in segments])
print("echo -i {} -vn -c copy -f segment -segment_times {} {}/output%d.mp3".format(args.input, timestamps_string, args.output))
result = subprocess.run("ffmpeg -i {} -vn -c copy -f segment -segment_times {} {}/output%d.mp3".format(args.input, timestamps_string, args.output), capture_output=True, shell=True)
print(result.stdout)
print(result.stderr)
artist = input('Artist: ')
album = input('Album: ')
year = input('Year: ')
for i, segment in enumerate(segments):
    # The first split is before the first timestamp and not associated with any track, we skip it
    split_num = i + 1
    track_num = split_num
    if args.skip_intro:
        track_num = track_num - 1
        if split_num == 1:
            continue
    output_filename = "{} - {}.mp3".format(str(track_num).zfill(2), segment["title"])
    os.rename(os.path.join(args.output, "output{}.mp3".format(split_num)),
              os.path.join(args.output, output_filename))
    print("{} -> {}".format("output{}.mp3".format(split_num), output_filename))
    result = subprocess.run('mid3v2 --track="{}" --year="{}" --song="{}" --album="{}" --artist="{}" "{}"'.format(
        str(track_num + 1).zfill(2), year, segment["title"], album, artist, os.path.join(args.output, output_filename)),
        capture_output=True, shell=True)
    if result.stderr:
        print(result.stderr)


os.remove(os.path.join(args.output, "output0.mp3"))
if args.skip_intro:
    os.remove(os.path.join(args.output, "output1.mp3"))
